Proteome Motif Finder
=====================
A simple tool to mine FASTA-formatted proteomes for REGEX motif patterns.  

Project Correspondents:
-----------------------
Dr. Maria Gallegos  
California State University, East Bay
