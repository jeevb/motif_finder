class Settings(object):

    # Regular expression pattern of motif to search for
    # Default: HX(K/R)XX(S/T)
    #   motif = r'H.[K|R]..[S|T]'
    motif = r'H.[K|R]..[S|T]'

    # Regular expression pattern of index in FASTA headers
    header = r'parent=.*(FBgn\d+)'

    # Path to FASTA file containing protein sequences
    proteome = '/Users/sanjeev/Workspace/temp/dmel-all-translation-r6.03.fasta'

    # Path to write out results in CSV format
    output = '/Users/sanjeev/Workspace/temp/test.csv'
