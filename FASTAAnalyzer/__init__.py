from Bio import SeqIO
from collections import defaultdict
from FASTARecord import FASTARecord


class FASTAAnalyzer(object):
    """An object to process specified FASTA file and store results."""
    def __init__(self, settings):
        self.settings = settings
        self.motifs = defaultdict(set)

    def _mine_motifs(self, fasta):
        """Mine motifs from specified fasta file."""
        for i in SeqIO.parse(open(fasta, 'rU'), 'fasta'):
            record = FASTARecord(i, self.settings.header, self.settings.motif)
            for motif in record.motifs():
                self.motifs[record.index].add(motif)

    def _write_motifs(self, out):
        """Write out mined motifs to specified file."""
        output = open(out, 'w')
        try:
            output.write('Index,Motif\n')
            for index, motifs in self.motifs.iteritems():
                output.writelines(
                    '%s,%s\n' % (index, motif) for motif in motifs
                )
        finally:
            output.close()

    def run(self):
        """Run the analyzer."""
        self._mine_motifs(self.settings.proteome)
        self._write_motifs(self.settings.output)
