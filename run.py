#!/usr/bin/env python

from FASTAAnalyzer import FASTAAnalyzer
from settings import Settings

def main():
    analyzer = FASTAAnalyzer(Settings())
    analyzer.run()

if __name__ == '__main__':
    main()
