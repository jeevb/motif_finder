import re


class FASTARecord(object):
    """An extension of the SeqRecord object."""
    def __init__(self, record, header, motif):
        self.record = record
        self.header = header
        self.motif = motif

    @property
    def index(self):
        """Devise an index based on the header of a FASTA record
        using the REGEX pattern specified in the settings file.
        """
        match = re.search(self.header, self.record.description)
        return match.group(1) if match else 'Unknown'

    def motifs(self):
        """Find matches for the motif REGEX pattern specified in the
        settings file in the current FASTA record.
        """
        for match in re.finditer(self.motif, str(self.record.seq)):
            yield match.group()
